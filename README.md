This is a shell repository providing the two core servers of a MML Anaesthetic
Deparment sandpit via docker:

* node
* postgres

Two seperate node repos exist:

      * "node_app" for express/sequalise middleware
      * "aps" for angular/boostrap/gulp front-end

### Generic middleware - provide a client facing API

      http://localhost:3030/admin/status




The app makes its available via port 3000 which the docker-compose file maps to:

    http://127.0.0.1:3030/

Copy the built version of the front end to the middleware repo with:

    cp aps/dist node_app/express/public/aps -r

This makes the app available:

    http://localhost:3030/aps

# Todo

* front end log out
* display only date of birth day

* script so deploy angular UI statically
* front end ability to record referal
* front end ability to record discharge
* front end ability to record assessment
* seperate test data

achieved:
* install [pg-session](https://www.npmjs.com/package/connect-pg-simple)
* service to log in
* front end log in
* logged in only routes
* secret session view
* install sequalize
* create patient model
* create review model
* REST ability to add a patient
* REST add review
* REST discharge
* list patients via UI
* comprehensive test script of middleware
* move from environment variables for config to json


### APS front end


## Installation

1. install `git`, `docker` and [`docker-compose`](https://docs.docker.com/compose/install/)
2. clone down this repo: `git clone https://bitbucket.org/erichbschulz/mmla`
3. initialise the git sub-modules: `git submodule init && git submodule update`

If the submodule fails it should be possible to simply clone the `mmlan` repo into the the node_app directory.

### build and start the containers:

    # move to correct directory
    cd mmla
    cd docker
    # copy package.json to enable initial build or rebuild
    cp ../node_app/express/package.json nodejs
    # build
    docker-compose build
    # run the create db script (from /node_app/express/models)
    docker exec -i docker_nodejs_1 node models/database.js

### start up the containers

    docker-compose up

### reset the db:

    docker exec -i docker_nodejs_1 node models/database.js

## scripts

### Editing

      vim -S bin/mmla.vim

### set up the path to use the scripts in the bin:

      PATH=/home/erich/mater/mmla/bin;$PATH

### open a shell on the node server:

    docker ps
    docker exec -it docker_nodejs_1 bash
    docker exec -i docker_nodejs_1 ls

    docker exec -it docker_postgresdb_1 psql -U postgres postgres
    docker exec -i docker_nodejs_1 ls models

### run a node script

    # ie from /node_app/express/models/database.js
    docker exec -i docker_nodejs_1 node models/database.js

### check dependancies

    git --version
    docker --version
    docker-compose --version

# Technologies in use:

* git, git submodules
* docker, docker-compose
* node
* postgres
* gulp
* express, [generator](https://www.npmjs.com/package/generator-express)


Blog:

Basic postgres crud based on this [tute](http://mherman.org/blog/2015/02/12/postgresql-and-nodejs/#.Vwwq1WcrKkA).

Split of development and and app repos using this [tute](https://www.airpair.com/docker/posts/efficiant-development-workfow-using-git-submodules-and-docker-compose)

    git submodule add --name mmlan git@bitbucket.org:erichbschulz/mmlan.git node_app

Installed session module and created table to record sessions

Shifted key configuration to use environmental variables

Set up logging of configuration of start up

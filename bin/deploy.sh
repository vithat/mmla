#!/usr/bin/env bash

BASE=$(dirname "$(readlink -f "$0")")/..
DEV=aps/dist
STAGE=node_app/express/public/aps
echo "copying files from $DEV to $STAGE"
cp $BASE/$DEV $BASE/$STAGE -rf


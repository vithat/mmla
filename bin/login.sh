#!/usr/bin/env bash

. $(dirname "$(readlink -f "$0")")/config.sh
echo "logging into $HOST and saving cookies to $COOKIES"
curl -s -c $COOKIES --data "user=foo&pass=bart" $HOST/user/login

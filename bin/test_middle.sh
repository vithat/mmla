#!/usr/bin/env bash

#### setup
# current path
P=$(dirname "$(readlink -f "$0")")
. $P/config.sh
# curl with cookies:
CURLA="$P/cm.sh"
JSON=""

#Q='{"name":"Mr Burns","role":"admin"}'
#$CURLA -X PUT -H '"Content-Type: application/json"' -d $Q $HOST/admin/user/61232

## test
function test {
  echo "// testing $HOST"
  echo "//reset aps: 1/0";
  summary "$($CURLA $HOST/admin/reset)"
  echo "//testing aps 3/0";
  summary "$(test_aps)"
  $CURLA $API/patient/155
  return

  $CURLA $HOST/admin/users
  echo "//testing users 7/1";
  summary "$(test_users)"
  $CURLA $HOST/admin/users
  echo "//testing item 8/0";
  summary "$(test_item_CRUD)"
}

function summary {
  RESULT="$1"
  SUCCESS='"success": true'
  FAIL='"success": false'
  N_SUCCESS=$(echo "$RESULT" | grep "$SUCCESS" | wc -l)
  N_FAIL=$(echo "$RESULT" | grep "$FAIL" | wc -l)
  echo "passess/fails $N_SUCCESS/$N_FAIL"
}

function test_aps {
  $CURLA -X POST --data "user_id=61231&dob=1967-9-7&name=barnaby&operation=caesar" $API/patient/155/refer
  $CURLA -X POST --data "user_id=61231&nausea=4&pain_movement=6&pain_rest=2" $API/patient/155/assess
  $CURLA -X POST --data "user_id=61231" $API/patient/155/discharge
}

function test_status {
  $CURLA $HOST/admin/status
  $CURLA $API/patients
  $CURLA $HOST/admin/users
}

function test_users {
  $CURLA $HOST/admin/users
  # dispatch gramps:
  $CURLA -X DELETE $HOST/admin/user/61231
  # add marge
  $CURLA -X PUT --data "name=Marge&role=admin" $HOST/admin/user/61236
  $CURLA -X PUT --data "role=user" $HOST/admin/user/61236
  $CURLA -X PUT --data "name=Homer" $HOST/admin/user/61233
  $CURLA -X PUT --data "name=Burns" $HOST/admin/user/61239
  $CURLA -X PUT --data "role=admin" $HOST/admin/user/61239
  # should fail as 61231 is deleted
  $CURLA -X PUT --data "role=admin" $HOST/admin/user/61231
}

function test_item_CRUD {
# create
  $CURLA --data "text=test1&complete=false" $API/item
  $CURLA --data "text=test2&complete=false" $API/item
  $CURLA --data "text=test3&complete=false" $API/item
  $CURLA --data "text=test4&complete=false" $API/item
# update:
  $CURLA -X PUT --data "text=fred&complete=true" $API/item/1
  $CURLA -X PUT --data "text=bill&complete=true" $API/item/2
# delete:
  $CURLA -X DELETE $API/item/3
# read
  $CURLA $API/item/1
}

test

#!/usr/bin/env bash

# current path
P=$(dirname "$(readlink -f "$0")")

cd $P/../docker
docker-compose up
